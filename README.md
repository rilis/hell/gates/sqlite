## Sqlite proxy repository for hell

This is proxy repository for [Sqlite library](https://www.sqlite.org/), which allow you to build and install it  using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of Sqlite using hell, have improvement idea, or want to request support for other versions of Sqlite, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in Sqlite itself please create issue on [Sqlite support page](https://www.sqlite.org/support.html), because here we don't do any kind of Sqlite development.
